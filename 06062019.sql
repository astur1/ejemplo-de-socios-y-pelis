﻿DROP DATABASE IF EXISTS b20190606;
CREATE DATABASE IF NOT EXISTS b20190606;
USE b20190606;
CREATE TABLE socio (
codigo int AUTO_INCREMENT,
  nombre varchar (20),
  PRIMARY KEY (codigo)
);
  CREATE TABLE telefonos (
  socio int,
    telefono varchar (20),
PRIMARY KEY (socio,telefono),
    CONSTRAINT telefsocio FOREIGN KEY (socio) REFERENCES socio (codigo)
  );
  CREATE TABLE email (
  socio int,
    email varchar (30),
    PRIMARY KEY (socio, email),
    CONSTRAINT emailsocio FOREIGN KEY (socio) REFERENCES socio (codigo)
  );
  CREATE TABLE peliculas (
  codpeli int AUTO_INCREMENT,
    titulo varchar (20),
    PRIMARY KEY (codpeli)
  );
  CREATE TABLE alquila (
    codigo int,
    codpeli int,
    PRIMARY KEY (codigo, codpeli),
    CONSTRAINT alquisocio FOREIGN KEY (codigo) REFERENCES socio (codigo),
    CONSTRAINT alquipeli FOREIGN KEY (codpeli) REFERENCES peliculas (codpeli)
    );
  CREATE TABLE fecha (
  codigo int,
    codpeli int,
    fecha date,
    PRIMARY KEY (codigo, codpeli, fecha),
    CONSTRAINT fechalquila FOREIGN KEY (codigo, codpeli) REFERENCES alquila (codigo, codpeli)
  ); 